<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM1840EF</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_009571</span>
<span class="globalSectionName">2TR-FBE ENGINE CONTROL</span>
<span class="globalTitle">_0052126</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>2TR-FBE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P2102&nbsp;&nbsp;Throttle Actuator Control Motor Circuit Low&nbsp;&nbsp;P2103&nbsp;&nbsp;Throttle Actuator Control Motor Circuit High&nbsp;&nbsp;</h1>
<br>
<div id="RM1000000004N56_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The throttle actuator is operated by the ECM and opens and closes the throttle valve using gears.
</p>
<p>The opening angle of the throttle valve is detected by the throttle position sensor, which is built into the throttle body. The throttle position sensor provides feedback to the ECM. This feedback allows the ECM to appropriately control the throttle actuator and monitor the throttle opening angle as the ECM responds to driver inputs.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>This ETCS (Electronic Throttle Control System) does not use a throttle cable.
</p>
</dd>
</dl>
<br>
<table summary="dtc-explan-table">
<colgroup>
<col style="width:10%">
<col style="width:15%">
<col style="width:26%">
<col style="width:26%">
<col style="width:10%">
<col style="width:10%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC No.
</th>
<th class="alcenter">Detection Item
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
<th class="alcenter">MIL
</th>
<th class="alcenter">Memory
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alleft">P2102
</td>
<td class="alleft">Throttle Actuator Control Motor Circuit Low
</td>
<td class="alleft">Both conditions are met for 2.0 seconds (1 trip detection logic):
<br>
(a) The throttle actuator duty ratio is 80% or more.
<br>
(b) The throttle actuator current is 0.5 A or less.
</td>
<td class="alleft"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Open in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle body with motor assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
<td class="alleft">Comes on
</td>
<td class="alleft">DTC stored
</td>
</tr>
<tr>
<td class="alleft">P2103
</td>
<td class="alleft">Throttle Actuator Control Motor Circuit High
</td>
<td class="alleft">Either condition is met (1 trip detection logic):
<br>
<div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The throttle actuator current is 10 A or higher for 0.1 seconds.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The throttle actuator current is 7 A or higher for 0.6 seconds.
</p>
</div>
</div>
</div>
<br>
</td>
<td class="alleft"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Short in throttle actuator circuit
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Throttle body with motor assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
<td class="alleft">Comes on
</td>
<td class="alleft">DTC stored
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<div id="RM1000000004N56_10" class="category no03">
<h2>FAIL-SAFE</h2>
<div class="content5">
<p>When either of these DTCs as well as other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator and the throttle valve is returned to a 6.5° opening angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel-cut) and ignition timing in accordance with the accelerator pedal position to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly. The ECM continues operating in fail-safe mode until a pass condition is detected and the ignition switch is turned off.
</p>
</div>
</div>
<div id="RM1000000004N56_07" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/A226209.png" alt="A226209" title="A226209">
<div class="indicateinfo">
<span class="caption">
<span class="points">0.063,2.51 0.438,2.75</span>
<span class="captionSize">0.375,0.24</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C67</span>
</span>
<span class="caption">
<span class="points">0.052,2.698 2.26,3.01</span>
<span class="captionSize">2.208,0.313</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Throttle Body with Motor Assembly</span>
</span>
<span class="caption">
<span class="points">2.74,2.594 3.438,2.854</span>
<span class="captionSize">0.698,0.26</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Shielded</span>
</span>
<span class="caption">
<span class="points">1.135,1.146 1.427,1.365</span>
<span class="captionSize">0.292,0.219</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">1.156,1.875 1.448,2.104</span>
<span class="captionSize">0.292,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">1.448,1.146 1.604,1.323</span>
<span class="captionSize">0.156,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">2</span>
</span>
<span class="caption">
<span class="points">1.448,1.875 1.604,2.052</span>
<span class="captionSize">0.156,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">1</span>
</span>
<span class="caption">
<span class="points">5.802,3.313 6.333,3.51</span>
<span class="captionSize">0.531,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">ECM</span>
</span>
<span class="caption">
<span class="points">4.594,1.052 4.844,1.229</span>
<span class="captionSize">0.25,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">14</span>
</span>
<span class="caption">
<span class="points">4.594,1.802 4.896,2.063</span>
<span class="captionSize">0.302,0.26</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">15</span>
</span>
<span class="caption">
<span class="points">4.604,2.563 4.833,2.74</span>
<span class="captionSize">0.229,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">24</span>
</span>
<span class="caption">
<span class="points">4.896,1.156 5.219,1.438</span>
<span class="captionSize">0.323,0.281</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M+</span>
</span>
<span class="caption">
<span class="points">4.906,1.896 5.177,2.115</span>
<span class="captionSize">0.271,0.219</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">M-</span>
</span>
<span class="caption">
<span class="points">4.896,2.75 5.427,3.021</span>
<span class="captionSize">0.531,0.271</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">GE01</span>
</span>
<span class="caption">
<span class="points">5.354,1.271 6.51,1.74</span>
<span class="captionSize">1.156,0.469</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Throttle Actuator Control Circuit</span>
</span>
<span class="caption">
<span class="points">4.552,1.25 4.927,1.479</span>
<span class="captionSize">0.375,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C98</span>
</span>
<span class="caption">
<span class="points">4.542,2 4.969,2.271</span>
<span class="captionSize">0.427,0.271</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C98</span>
</span>
<span class="caption">
<span class="points">4.542,2.76 4.865,2.938</span>
<span class="captionSize">0.323,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">C97</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
<div id="RM1000000004N56_08" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>The throttle actuator current (Throttle Motor Current) and throttle actuator duty ratio (Throttle Motor Duty (Open) / Throttle Motor Duty (Close)) can be read using the intelligent tester. However, the ECM shuts off the throttle actuator current when the ETCS malfunctions.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM1000000004N56_12" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM1000000004N56_12_0001">
<div class="testtitle"><span class="titleText">1.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Inspect the throttle body with motor assembly.
</p>
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;THROTTLE BODY&gt;INSPECTION<span class="invisible">201110,999999,_51,_009571,_0052113,RM1000000004N20,</span></a>
</p>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000004N56_12_0001_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE BODY WITH MOTOR ASSEMBLY - ECM)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201110,201310,_51,_009571,_0052113,RM1000000004N22,</span><span class="invisible">201310,999999,_51,_009571,_0052113,RM100000000AH5Z,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000004N56_12_0003">
<div class="testtitle"><span class="titleText">2.CHECK HARNESS AND CONNECTOR (THROTTLE BODY WITH MOTOR ASSEMBLY - ECM)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Disconnect the throttle body with motor assembly connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Disconnect the ECM connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Measure the resistance according to the value(s) in the table below.
</p>
<dl class="spec">
<dt class="spec">Standard Resistance (Check for Open):</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">C67-2 (M+) - C98-14 (M+)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
<tr>
<td class="alcenter">C67-1 (M-) - C98-15 (M-)
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">Below 1 Ω
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
<dl class="spec">
<dt class="spec">Standard Resistance (Check for Short):</dt>
<dd class="spec"><table summary="" class="half">
<colgroup>
<col style="width:33%">
<col style="width:33%">
<col style="width:33%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Connection
</th>
<th class="alcenter">Condition
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">C67-2 (M+) or C98-14 (M+) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
<tr>
<td class="alcenter">C67-1 (M-) or C98-15 (M-) - Body ground
</td>
<td class="alcenter">Always
</td>
<td class="alcenter">10 kΩ or higher
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Reconnect the throttle body with motor assembly connector.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Reconnect the ECM connector.
</p>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000004N56_12_0003_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (VISUALLY CHECK THROTTLE VALVE)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPAIR OR REPLACE HARNESS OR CONNECTOR (THROTTLE BODY WITH MOTOR ASSEMBLY - ECM)</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000004N56_12_0005">
<div class="testtitle"><span class="titleText">3.INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (VISUALLY CHECK THROTTLE VALVE)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check for foreign objects between the throttle valve and housing.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>No foreign objects between throttle valve and housing.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000004N56_12_0005_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">4.CHECK THROTTLE BODY WITH MOTOR ASSEMBLY (THROTTLE VALVE)</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REMOVE FOREIGN OBJECT AND CLEAN THROTTLE BODY</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000004N56_12_0010">
<div class="testtitle"><span class="titleText">4.CHECK THROTTLE BODY WITH MOTOR ASSEMBLY (THROTTLE VALVE)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Check if the throttle valve opens and closes smoothly.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>Throttle valve opens and closes smoothly.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000004N56_12_0010_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>OK
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE ECM</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;ECM&gt;REMOVAL<span class="invisible">201110,999999,_51,_009571,_0052114,RM1000000004N26,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;THROTTLE BODY&gt;REMOVAL<span class="invisible">201110,201310,_51,_009571,_0052113,RM1000000004N22,</span><span class="invisible">201310,999999,_51,_009571,_0052113,RM100000000AH5Z,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
