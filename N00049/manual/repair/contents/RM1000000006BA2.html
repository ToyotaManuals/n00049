<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM1840EF</span>
<span class="globalServcat">_51</span>
<span class="globalServcatName">Engine / Hybrid System</span>
<span class="globalSection">_009571</span>
<span class="globalSectionName">2TR-FBE ENGINE CONTROL</span>
<span class="globalTitle">_0052126</span>
<span class="globalTitleName">SFI SYSTEM</span>
<span class="globalCategory">C</span>
</div>
<h1>2TR-FBE ENGINE CONTROL&nbsp;&nbsp;SFI SYSTEM&nbsp;&nbsp;P0016&nbsp;&nbsp;Crankshaft Position - Camshaft Position Correlation (Bank 1 Sensor A)&nbsp;&nbsp;</h1>
<br>
<div id="RM1000000006BA2_01" class="category no03">
<h2>DESCRIPTION</h2>
<div class="content5">
<p>The Variable Valve Timing (VVT) system controls the intake camshaft and exhaust camshaft to achieve optimal valve timing according to various driving conditions. This control is performed based on the intake air volume, throttle valve position, engine coolant temperature and other engine operating conditions. The ECM controls the camshaft timing oil control valves based on the signals transmitted by several sensors. As a result, the relative positions of the camshafts are optimized, improving engine torque and fuel economy and reducing exhaust emissions. In addition, the ECM detects the actual valve timing based on the signals from the VVT sensors and performs feedback control, achieving optimal valve timing.
</p>
<table summary="dtc-explan-table">
<colgroup>
<col style="width:10%">
<col style="width:15%">
<col style="width:26%">
<col style="width:26%">
<col style="width:10%">
<col style="width:10%">
</colgroup>
<thead>
<tr>
<th class="alcenter">DTC No.
</th>
<th class="alcenter">Detection Item
</th>
<th class="alcenter">DTC Detection Condition
</th>
<th class="alcenter">Trouble Area
</th>
<th class="alcenter">MIL
</th>
<th class="alcenter">Memory
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alleft">P0016
</td>
<td class="alleft">Crankshaft Position - Camshaft Position Correlation (Bank 1 Sensor A)
</td>
<td class="alleft">A deviation in the crankshaft position sensor signal and VVT sensor (bank 1) signal (2 trip detection logic).
</td>
<td class="alleft"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Valve timing
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Camshaft timing oil control valve assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Oil control valve filter
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Camshaft timing gear assembly
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>ECM
</p>
</div>
</div>
</div>
<br>
</td>
<td class="alleft">Comes on
</td>
<td class="alleft">DTC stored
</td>
</tr>
</tbody>
</table>
<br>
<p>To monitor the correlation of the intake camshaft position and crankshaft position, the ECM checks the VVT learned value while the engine is idling. The VVT learned value is calibrated based on the camshaft position and crankshaft position. The intake valve timing is set to the most retarded angle while the engine is idling. If the VVT learned value is out of the specified range in consecutive driving cycles, the ECM illuminates the MIL and stores DTC P0016.
</p>
</div>
</div>
<div id="RM1000000006BA2_06" class="category no03">
<h2>WIRING DIAGRAM</h2>
<div class="content5">
<p>Refer to DTC P0010 (See page <a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;P0010<span class="invisible">201210,999999,_51,_009571,_0052126,RM1000000006BA0,P0010</span></a>).
</p>
</div>
</div>
<div id="RM1000000006BA2_07" class="category no10">
<h2>CAUTION / NOTICE / HINT</h2>
<div class="content5">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.
</p>
</dd>
</dl>
<br>
</div>
</div>
<div id="RM1000000006BA2_09" class="category no01">
<h2>PROCEDURE</h2>
<div class="testgrp" id="RM1000000006BA2_09_0001">
<div class="testtitle"><span class="titleText">1.CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0016)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the intelligent tester to the DLC3.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the tester on.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Enter the following menus: Powertrain / Engine and ECT / DTC.
</p>
<dl class="gtsDtcRead">
<dt class="gtsDtcRead">Powertrain &gt; Engine and ECT &gt; Trouble Codes</dt>
<dd class="gtsDtcRead gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50001</span>
<span class="gtsEcu">234</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Read DTCs.
</p>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0001_1" summary="" class="half">
<colgroup>
<col style="width:50%">
<col style="width:49%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">DTC P0016 is output
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td class="alcenter">DTC P0016 and other DTCs are output
</td>
<td class="alcenter">B
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>If any DTCs other than P0016 are output, troubleshoot those DTCs first.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueNext"><p>A
</p></div>
<div class="nextAction"><span class="titleText">2.PERFORM ACTIVE TEST USING INTELLIGENT TESTER (OPERATE CAMSHAFT TIMING OIL CONTROL VALVE)</span></div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">GO TO DTC CHART</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;DIAGNOSTIC TROUBLE CODE CHART<span class="invisible">201210,999999,_51,_009571,_0052126,RM1000000004N3K,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000006BA2_09_0003">
<div class="testtitle"><span class="titleText">2.PERFORM ACTIVE TEST USING INTELLIGENT TESTER (OPERATE CAMSHAFT TIMING OIL CONTROL VALVE)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the intelligent tester to the DLC3.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Start the engine.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the tester on.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the VVT Linear (Bank 1).
</p>
<dl class="gtsActiveTest">
<dt class="gtsActiveTest">Powertrain &gt; Engine and ECT &gt; Active Test</dt>
<dd class="gtsActiveTest">
<table summary="active-test" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Display
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alleft">Control the VVT Linear (Bank1)
</td>
</tr>
</tbody>
</table>
</dd>
<dd class="gtsActiveTest gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50004</span>
<span class="gtsEcu">234</span>
<span class="gtsDid">32</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Check the engine speed while operating the camshaft timing oil control valve using the intelligent tester.
</p>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4"><div class="list1">
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>When performing the Active Test, make sure the air conditioning is on.
</p>
</div>
</div>
<div class="list1Item">
<div class="list1Head">&middot;</div>
<div class="list1Body"><p>Make sure the engine coolant temperature when the engine is started is 30°C (86°F) or less.
</p>
</div>
</div>
</div>
<br>
</dd>
</dl>
<br>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><table summary="">
<colgroup>
<col style="width:49%">
<col style="width:50%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Operation
</th>
<th class="alcenter">Specified Condition
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">0%
</td>
<td class="alcenter">Normal engine speed
</td>
</tr>
<tr>
<td class="alcenter">100%
</td>
<td class="alcenter">Engine idles roughly or stalls
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>Refer to "Data List / Active Test" [VVT OCV Duty #1 and VVT Change Angle #1].
</p>
</dd>
<dd class="atten4">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;DATA LIST / ACTIVE TEST<span class="invisible">201110,999999,_51,_009571,_0052126,RM1000000004N63,</span></a>
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0003_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">3.CHECK WHETHER DTC OUTPUT RECURS (DTC P0016)</span></div>
<div class="judgeValueNext"><p>NG
</p></div>
<div class="nextAction"><span class="titleText">4.INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</span></div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000006BA2_09_0011">
<div class="testtitle"><span class="titleText">3.CHECK WHETHER DTC OUTPUT RECURS (DTC P0016)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the intelligent tester to the DLC3.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the tester on.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Clear the DTCs.
</p>
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;DTC CHECK / CLEAR<span class="invisible">201210,999999,_51,_009571,_0052126,RM1000000004N5Y,</span></a>
</p>
<dl class="gtsDtcDelete">
<dt class="gtsDtcDelete">Powertrain &gt; Engine and ECT &gt; Clear DTCs</dt>
<dd class="gtsDtcDelete gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50002</span>
<span class="gtsEcu">234</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Start the engine and warm it up.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">f.</div>
<div class="test1Body">
<p>Switch the ECM from normal mode to check mode using the tester.
</p>
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;CHECK MODE PROCEDURE<span class="invisible">201110,999999,_51,_009571,_0052126,RM1000000004N5Z,</span></a>
</p>
<dl class="gtsUtilityNormal">
<dt class="gtsUtilityNormal">Powertrain &gt; Engine and ECT &gt; Utility</dt>
<dd class="gtsUtilityNormal">
<table summary="utility" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Display
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alleft">Check Mode
</td>
</tr>
</tbody>
</table>
</dd>
<dd class="gtsUtilityNormal gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50005</span>
<span class="gtsEcu">234</span>
<span class="gtsUtil">1</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">g.</div>
<div class="test1Body">
<p>Allow the engine to idle for more than 1 minute.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">h.</div>
<div class="test1Body">
<p>Drive the vehicle for more than 10 minutes.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">i.</div>
<div class="test1Body">
<p>Enter the following menus: Powertrain / Engine and ECT / DTC.
</p>
<dl class="gtsDtcRead">
<dt class="gtsDtcRead">Powertrain &gt; Engine and ECT &gt; Trouble Codes</dt>
<dd class="gtsDtcRead gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50001</span>
<span class="gtsEcu">234</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">j.</div>
<div class="test1Body">
<p>Read the DTCs.
</p>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0011_1" summary="" class="half">
<colgroup>
<col style="width:49%">
<col style="width:50%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">DTC is not output
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td class="alcenter">DTC P0016 is output
</td>
<td class="alcenter">B
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>DTC P0016 is stored when foreign objects in the engine oil are caught in some parts of the system. This code remains stored for a short time even after the system returns to normal. These foreign objects may then be captured by the oil control valve filter, thus eliminating the source of the problem.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>A
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">CHECK FOR INTERMITTENT PROBLEMS</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;CHECK FOR INTERMITTENT PROBLEMS<span class="invisible">201210,999999,_51,_009571,_0052126,RM1000000004N66,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueNext"><p>B
</p></div>
<div class="nextAction"><span class="titleText">7.ADJUST VALVE TIMING</span></div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000006BA2_09_0015">
<div class="testtitle"><span class="titleText">4.INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Inspect the camshaft timing oil control valve assembly.
</p>
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;CAMSHAFT OIL CONTROL VALVE&gt;INSPECTION<span class="invisible">201110,999999,_51,_009571,_0052112,RM1000000004N1V,</span></a>
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item test1HasFigure">
<div class="test1Head">b.</div>
<div class="test1Body">
<div class="figureAPatternGroup">
<div class="figure">
<div class="aPattern">
<div class="graphic">
<img src="../img/png/A183795E21.png" alt="A183795E21" title="A183795E21">
<div class="indicateinfo">
<span class="caption">
<span class="points">0.135,0.094 2.615,0.313</span>
<span class="captionSize">2.479,0.219</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Component without harness connected:</span>
</span>
<span class="caption">
<span class="points">0.146,0.281 2.333,0.719</span>
<span class="captionSize">2.188,0.438</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">(Camshaft Timing Oil Control Valve Assembly)</span>
</span>
<span class="caption">
<span class="points">0.219,1.323 1.531,1.552</span>
<span class="captionSize">1.313,0.229</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Valve moves quickly</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
<p>Connect the positive (+) battery terminal to terminal 1 and the negative (-) battery terminal to terminal 2. Check the valve operation.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>Valve moves quickly.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Reinstall the camshaft timing oil control valve assembly.
</p>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0015_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">5.INSPECT CAMSHAFT TIMING GEAR ASSEMBLY</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;CAMSHAFT OIL CONTROL VALVE&gt;REMOVAL<span class="invisible">201110,999999,_51,_009571,_0052112,RM1000000004N1X,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000006BA2_09_0016">
<div class="testtitle"><span class="titleText">5.INSPECT CAMSHAFT TIMING GEAR ASSEMBLY</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Inspect the camshaft timing gear assembly.
</p>
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE MECHANICAL&gt;CAMSHAFT&gt;REMOVAL<span class="invisible">201210,999999,_51,_009576,_0052195,RM1000000004NK8,</span></a>
</p>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0016_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">6.INSPECT OIL CONTROL VALVE FILTER</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE CAMSHAFT TIMING GEAR ASSEMBLY</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE MECHANICAL&gt;CAMSHAFT&gt;REMOVAL<span class="invisible">201210,999999,_51,_009576,_0052195,RM1000000004NK8,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000006BA2_09_0007">
<div class="testtitle"><span class="titleText">6.INSPECT OIL CONTROL VALVE FILTER</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Remove the oil control valve filter.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Check that the filter is not clogged.
</p>
<dl class="spec">
<dt class="spec">OK:</dt>
<dd class="spec"><p>Filter is not clogged.
</p>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Reinstall the oil control valve filter.
</p>
</div>
</div>
</div>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0007_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">OK
</td>
</tr>
<tr>
<td class="alcenter">NG
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>OK
</p></div>
<div class="nextAction"><span class="titleText">7.ADJUST VALVE TIMING</span></div>
<div class="judgeValueEnd"><p>NG
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE OIL CONTROL VALVE FILTER</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000006BA2_09_0004">
<div class="testtitle"><span class="titleText">7.ADJUST VALVE TIMING</span></div>
<div class="content6">
<dl class="atten4">
<dt class="atten4">HINT:</dt>
<dd class="atten4">
<p>There are no marks on the cylinder head to align for the purpose of checking valve timing. Valve timing can only be inspected by aligning the colored plates on the timing chain with the marks on the pulleys. It may be necessary to remove and reinstall the chain to align the timing marks.
</p>
</dd>
<dd class="atten4">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE MECHANICAL&gt;ENGINE UNIT&gt;REASSEMBLY<span class="invisible">201110,201410,_51,_009576,_0052200,RM1000000004NL4,</span><span class="invisible">201410,999999,_51,_009576,_0052200,RM100000000GGGL,</span></a>
</p>
</dd>
</dl>
<br>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0004_1" summary="" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">NEXT
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
<br>
<div class="judgeValueNext"><p>NEXT
</p></div>
<div class="nextAction"><span class="titleText">8.CHECK WHETHER DTC OUTPUT RECURS (DTC P0016)</span></div>
<div class="floatClear"></div>
</div>
<div class="testgrp" id="RM1000000006BA2_09_0018">
<div class="testtitle"><span class="titleText">8.CHECK WHETHER DTC OUTPUT RECURS (DTC P0016)</span></div>
<div class="content6">
<div class="test1">
<div class="test1Item">
<div class="test1Head">a.</div>
<div class="test1Body">
<p>Connect the intelligent tester to the DLC3.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">b.</div>
<div class="test1Body">
<p>Turn the ignition switch to ON.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">c.</div>
<div class="test1Body">
<p>Turn the tester on.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">d.</div>
<div class="test1Body">
<p>Clear the DTCs.
</p>
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;DTC CHECK / CLEAR<span class="invisible">201210,999999,_51,_009571,_0052126,RM1000000004N5Y,</span></a>
</p>
<dl class="gtsDtcDelete">
<dt class="gtsDtcDelete">Powertrain &gt; Engine and ECT &gt; Clear DTCs</dt>
<dd class="gtsDtcDelete gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50002</span>
<span class="gtsEcu">234</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">e.</div>
<div class="test1Body">
<p>Start the engine and warm it up.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">f.</div>
<div class="test1Body">
<p>Switch the ECM from normal mode to check mode using the tester.
</p>
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;SFI SYSTEM&gt;CHECK MODE PROCEDURE<span class="invisible">201110,999999,_51,_009571,_0052126,RM1000000004N5Z,</span></a>
</p>
<dl class="gtsUtilityNormal">
<dt class="gtsUtilityNormal">Powertrain &gt; Engine and ECT &gt; Utility</dt>
<dd class="gtsUtilityNormal">
<table summary="utility" class="half">
<colgroup>
<col style="width:100%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Tester Display
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alleft">Check Mode
</td>
</tr>
</tbody>
</table>
</dd>
<dd class="gtsUtilityNormal gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50005</span>
<span class="gtsEcu">234</span>
<span class="gtsUtil">1</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">g.</div>
<div class="test1Body">
<p>Allow the engine to idle for more than 1 minute.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">h.</div>
<div class="test1Body">
<p>Drive the vehicle for more than 10 minutes.
</p>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">i.</div>
<div class="test1Body">
<p>Enter the following menus: Powertrain / Engine and ECT / DTC.
</p>
<dl class="gtsDtcRead">
<dt class="gtsDtcRead">Powertrain &gt; Engine and ECT &gt; Trouble Codes</dt>
<dd class="gtsDtcRead gtsExec">
<input class="gtsBtn" type="button" value="Execute">
<span class="gtsFunction">50001</span>
<span class="gtsEcu">234</span>
</dd>
</dl>
</div>
</div>
</div>
<br>
<div class="test1">
<div class="test1Item">
<div class="test1Head">j.</div>
<div class="test1Body">
<p>Read the DTCs.
</p>
<dl class="spec">
<dt class="spec">Result:</dt>
<dd class="spec"><table id="RM1000000006BA2_09_0018_1" summary="" class="half">
<colgroup>
<col style="width:49%">
<col style="width:50%">
</colgroup>
<thead>
<tr>
<th class="alcenter">Result
</th>
<th class="alcenter">Proceed to
</th>
</tr>
</thead>
<tbody>
<tr>
<td class="alcenter">DTC is not output
</td>
<td class="alcenter">A
</td>
</tr>
<tr>
<td class="alcenter">DTC P0016 is output
</td>
<td class="alcenter">B
</td>
</tr>
</tbody>
</table>
<br>
</dd>
</dl>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
<div class="judgeValueEnd"><p>A
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">END</span></div>
<div class="content6">
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="judgeValueEnd"><p>B
</p></div>
<div class="endActBox">
<div class="endAction"><span class="titleText">REPLACE ECM</span></div>
<div class="content6">
<p>Click here<a class="mlink" href="javascript:void(0)">Engine / Hybrid System&gt;2TR-FBE ENGINE CONTROL&gt;ECM&gt;REMOVAL<span class="invisible">201110,999999,_51,_009571,_0052114,RM1000000004N26,</span></a>
</p>
</div>
<br>
<div class="floatClear"></div>
</div>
<div class="floatClear"></div>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
